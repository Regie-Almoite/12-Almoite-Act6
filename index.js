let userInput = prompt("Enter a number");

//Converts the userinput into an an integer and checks if it is a number
// If true it logs Unexpected Input and
// If false, checks the user input
// if it is an even number or odd
// number

function oddOrEven(value) {
    if (Number.isNaN(parseInt(value))) {
        console.log("Unexpected Input");
    } else {
        if (value % 2 == 0) {
            console.log(`${value} is an even number`);
        } else {
            console.log(`${value} is an odd number`);
        }
    }
}

oddOrEven(userInput);

oddOrEven(5);
oddOrEven(10);
oddOrEven("8");
oddOrEven("20");
oddOrEven("seven");
oddOrEven("fifteen");
oddOrEven(true);
oddOrEven(null);
oddOrEven(undefined);
oddOrEven(NaN);
